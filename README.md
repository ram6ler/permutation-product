# Permutation Product

A little demo of the [trotter library](https://bitbucket.org/ram6ler/dart_trotter) that calculates permutation compositions, or group products, on permutations of the letters of the alphabet input by the user.

