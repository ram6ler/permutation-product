//import 'dart:html';
import 'dart:html';

import 'package:trotter/trotter.dart';

final List<String> letters = characters('abcdefghijklmnopqrstuvwxyz');
final Permutations<String> permutations = letters.permutations();

// Elements
final DivElement pCyclic = querySelector('#p'),
    qCyclic = querySelector('#q'),
    pDisplay = querySelector('#p-display'),
    pqCyclic = DivElement(),
    qDisplay = querySelector('#q-display'),
    pqDisplay = querySelector('#pq-display');

void main() {
  {
    final replace = {
      'permutations': formatIndex(permutations.length),
      'first_and_last_ten': (() {
        final sb = StringBuffer();
        void show(BigInt from, BigInt to) {
          for (var i = from; i < to; i += BigInt.one) {
            sb
              ..write(asString(permutations[i])
                  .replaceFirst('z', '<span class="z">z</span>'))
              ..write('<br>');
          }
        }

        show(BigInt.zero, BigInt.from(10));
        sb.writeln('...<br>');
        show(permutations.length - BigInt.from(10), permutations.length);
        return sb.toString();
      })(),
      'memory_in_eb': formatIndex(permutations.length ~/
          BigInt.from(8) ~/
          BigInt.from(1024) ~/
          BigInt.from(1024) ~/
          BigInt.from(1024) ~/
          BigInt.from(1024) ~/
          BigInt.from(1024) ~/
          BigInt.from(1024)),
      'search_time_in_millennia': formatIndex(permutations.length ~/
          BigInt.one.pow(12) ~/
          BigInt.from(60) ~/
          BigInt.from(60) ~/
          BigInt.from(24) ~/
          BigInt.from(365) ~/
          BigInt.from(100)),
      'example_cyclic': '(ate)(cod)(fish)',
      'example_string': asString(interpret('(ate)(cod)(fish)')),
      'length_in_ly': formatIndex(
          permutations.length ~/ BigInt.from(300) ~/ BigInt.from(10).pow(8))
    };

    replace.forEach((divId, html) {
      querySelector('#$divId').innerHtml = html;
    });
  }
  pCyclic.contentEditable = 'true';
  qCyclic.contentEditable = 'true';
  pCyclic.onKeyUp.listen(update);
  qCyclic.onKeyUp.listen(update);

  update(null);
}

void update(KeyboardEvent _) {
  //pqCyclic.innerHtml = asCyclic(pq);
  final p = display(pCyclic, pDisplay), q = display(qCyclic, qDisplay);
  if (p != null && q != null) {
    final pq = product(p, q);
    pqCyclic.innerText = asCyclic(pq);
    display(pqCyclic, pqDisplay);
  } else {
    pqDisplay.innerHtml = '''
<div class='console error'>
<p>Check input!</p>
</div>
    ''';
  }
}

List<String> display(DivElement input, DivElement output) {
  List<String> permutation;
  final text = input.innerText.toLowerCase().replaceAll(RegExp(r'[^a-z]'), ';'),
      chs = characters(text).where((ch) => ch != ';'),
      freqs = Map<String, int>.fromIterable(chs,
          value: (ch) => chs.where((c) => c == ch).length),
      sb = StringBuffer();

  if (freqs.values.any((v) => v > 1)) {
    sb.write('''
      <div class='console error'>
      <p>Not a valid permutation:</p>
      <ul>
      ''');
    freqs.forEach((ch, f) {
      if (f == 2) {
        sb.write('''<li>'$ch' occurs twice.</li>''');
      } else if (f > 2) {
        sb.write('''<li>'$ch' occurs $f times.</li>''');
      }
    });
    sb.writeln('</ul></div>');
  } else {
    permutation = interpret(text);
    final index = permutations.indexOf(permutation);

    sb.write(
        '<table><tr><th>Index</th><th>Permutation</th><th>Cyclic Form</th></tr>');
    final d = BigInt.from(3);
    for (var i = index - d; i < index + d; i += BigInt.one) {
      final p = permutations[i], c = i == index ? ' class="console"' : '';
      sb.writeln(
          '<tr$c><td>${formatIndex(i % permutations.length)}</td><td>${asString(p)}</td><td>${asCyclic(p)}</td></tr>');
    }
    sb.writeln('</table>');
  }
  output.innerHtml = sb.toString();
  return permutation;
}

List<String> product(List<String> p, List<String> q) {
  var result = List<String>.from(letters);
  for (var i = 0; i < q.length; i++) {
    final ch = q[i], index = letters.indexOf(ch);
    result[i] = p[index];
  }
  return result;
}

String asString(List<String> permutation) => permutation.join('');

String asCyclic(List<String> permutation) {
  final sb = StringBuffer();
  var items = <String>{};
  String cycle(int index) {
    final letterFrom = letters[index], letterTo = permutation[index];
    if (items.contains(letterFrom)) {
      return ';';
    }
    items.add(letterFrom);
    return letterFrom + cycle(letters.indexOf(letterTo));
  }

  for (var i = 0; i < permutation.length; i++) {
    final ring = cycle(i);
    sb.write(ring);
  }
  final result = sb
      .toString()
      .split(RegExp(';+'))
      .where((result) => result.length > 1)
      .map((result) => '($result)')
      .join('');

  return result == '' ? '()' : result;
}

List<String> interpret(String cyclic) {
  var chs = List<String>.from(letters);
  final split = cyclic
      .toLowerCase()
      .split(RegExp(r'[^a-z]+'))
      .where((result) => result.isNotEmpty);
  for (final mapping in split) {
    for (var i = 0; i < mapping.length; i++) {
      final index = letters.indexOf(mapping[i]),
          ch = mapping[(i + 1) % mapping.length];
      chs[index] = ch;
    }
  }
  return chs;
}

String formatIndex(BigInt index) {
  final sb = StringBuffer(), string = index.toString();
  for (var i = 0; i < string.length; i++) {
    sb.write(string[i]);
    final place = string.length - (i + 1);
    if (place > 0 && place % 3 == 0) {
      sb.write(',');
    }
  }
  return sb.toString();
}
